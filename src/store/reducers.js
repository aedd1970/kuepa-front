import { combineReducers } from 'redux'

const user = (state = [], action) => {
  switch (action.type) {
    case 'ADD_USER':
      return {
        ...state,
        data: action.data,
        isAuth: action.isAuth,
      }
      case 'LOGOUT':
        return {
          ...state,
          data: action.data,
          isAuth: action.isAuth,
        }
    default:
      return state
  }
}

export default combineReducers({
  user,
})