import React, { useCallback } from "react";
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Badge,
} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import NotificationsIcon from '@material-ui/icons/Notifications';
import useStyles from './style'
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { logout } from '../../store/actions';

const NavBar = ({ dispatch }) => {
    const classes = useStyles();
    const history = useHistory();
    const doLogout = useCallback(() => {
        dispatch(logout());
        setTimeout(() => {
            window.location.reload();
        }, 3000);
    }, []);
    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar className={classes.toolbar}>
                <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                    TEST KUEPA
                </Typography>
                <IconButton color="inherit" onClick={doLogout} >
                    <ExitToAppIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}

export default connect()(NavBar);