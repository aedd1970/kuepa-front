import { makeStyles } from '@material-ui/core';


export default makeStyles((theme) => ({
    appBar: {
      zIndex: theme.zIndex.appBar,
      width: `calc(100%)`,
    },
    toolbar: {
      justifyContent: 'space-between'
    }
  }));