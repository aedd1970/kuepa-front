import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { connect } from 'react-redux';
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import Navbar from './components/NavBar';
import Home from './routes/Home';
import Register from './routes/Register';
import Chat from './routes/Chat';
import Login from './routes/Login';


const App = ({user}) => {
  const init = user.isAuth ? Chat : Home;
  
  return (
    <div>
      <CssBaseline />
      <div>
        <Navbar />
        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={init} />
            <Route exact path='/register' component={Register} />
            <Route exact path='/login' component={Login} />
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(App)
