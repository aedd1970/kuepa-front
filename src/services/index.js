import axios from 'axios';

const API = 'https://apikuepa.herokuapp.com/api/';

export function UserRegister(name,email,type_user,password) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${API}register/user`,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
      data: JSON.stringify({
        name,
        email,
        type_user,
        password,
      }),
    })
      .then(response => resolve(response.data))
      .catch(error => reject(error));
  });
}

export function UserLogin(email,password) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url: `${API}login/auth`,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin': '*',
        },
        data: JSON.stringify({
          email,
          password
        }),
      })
        .then(response => resolve(response.data))
        .catch(error => reject(error));
    });
  }