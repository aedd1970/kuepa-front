import React, { useState, useCallback, useRef } from "react";
import {
    Avatar,
    Button,
    CssBaseline,
    TextField,
    FormControlLabel,
    Checkbox,
    Grid,
    Link,
    Container,
    Typography,
    CircularProgress,
    Select,
    MenuItem,
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import { UserRegister } from '../../services';
import { addUser } from '../../store/actions';


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    circularProgress: {
        color: theme.palette.secondary.main
    }
}));

const Register = ({ dispatch }) => {
    const classes = useStyles();
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [typeUser, setTypeUser] = useState('');
    const email = useRef();
    const password = useRef();
    const name = useRef();


    const handleChange = (event) => {
        setTypeUser(event.target.value);
    };

    const register = useCallback(() => {
        setIsLoading(true);
        UserRegister(name.current.value,email.current.value,typeUser,password.current.value)
        .then(data => {
             dispatch(addUser(data));
             history.push('/')
        })
        .catch(error => {
            console.log(error);
            alert(error.response.data.err[0].msg)
            setIsLoading(false);
        })
    }, [setIsLoading, name, email, password, typeUser, history, dispatch]);

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <form
                    className={classes.form}
                    onSubmit={(e) => {
                        e.preventDefault();
                        register();
                    }}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="name"
                                label="Nombre completo"
                                type="name"
                                id="name"
                                inputRef={name}
                                autoComplete="name"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                inputRef={email}
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                inputRef={password}
                                autoComplete="current-password"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Select
                                id="simple-select"
                                value={typeUser}
                                onChange={handleChange}
                                displayEmpty
                            >
                                <MenuItem value="" disabled>
                                    Tipo de usuario
                                </MenuItem>
                                <MenuItem value="estudiante">Estudiante</MenuItem>
                                <MenuItem value="moderador">Moderador</MenuItem>
                            </Select>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox value="allowExtraEmails" color="primary" />}
                                label="Acepto terminos y condiciones"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        {isLoading ? <CircularProgress size={24} thickness={5} className={classes.circularProgress} /> : "Registrar"}
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="/" variant="body2">
                                Ya tienes una cuenta? Inicia sesión.
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}

export default connect()(Register)